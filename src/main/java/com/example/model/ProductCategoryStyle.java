package com.example.model;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ProductCategoryStyle {
    SMALL("small"),
    LARGE("large"),
    HEADING("heading"),
    HIGHLIGHTS("highlights"),
    DEFAULT("default");

    private String code;

    ProductCategoryStyle(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @JsonCreator
    public static ProductCategoryStyle fromString(String name) {
        String n = name.toLowerCase();
        for (ProductCategoryStyle type : ProductCategoryStyle.values()) {
            if (type.code.equalsIgnoreCase(n)) {
                return type;
            }
        }
        throw new IllegalArgumentException("unknown category style: " + n);
    }
}
