package com.example.model;

public interface GameAdvisorContext {
    String brand();
    String platform();
    boolean isWeb();
    boolean isMobile();
    String brandDomain();
    String registrationCountry();
    String userId(); // ~ ID in external system
}
