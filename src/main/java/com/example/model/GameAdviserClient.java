package com.example.model;

import java.util.List;
import java.util.Optional;

public interface GameAdviserClient {
    List<CategoryAdvice> suggestLayout(GameAdvisorContext ctx, String profile) throws Exception;
    Optional<CategoryAdvice> suggestPlacement(GameAdvisorContext ctx, String productCategoryCode, String gameId) throws Exception;
}
