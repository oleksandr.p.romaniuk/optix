package com.example.model;

import java.util.List;
import java.util.Objects;

public class Placement {
    public String title;
    public List<GameObject> result;
    public PlacementInfo extra;

    public static class PlacementInfo {
        public String code;
        public String icon;
        public String route;
        public Integer lines;
        public String style = ProductCategoryStyle.DEFAULT.getCode();
        public String type = ProductCategoryStyle.DEFAULT.getCode();
        public String promotionUrl;

        public boolean hasInfo() {
            return Objects.nonNull(icon) && Objects.nonNull(route) && Objects.nonNull(lines);
        }
    }
}
