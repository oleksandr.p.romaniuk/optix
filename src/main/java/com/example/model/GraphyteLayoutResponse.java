package com.example.model;

import java.util.List;

public class GraphyteLayoutResponse {
    public Layout layout;

    public static class Layout {
        public List<AreaObject> result;
    }
}
