package com.example.model;

import java.util.List;

public class Recommendation {
    public List<Result> result;

    public static class Result {
        public GameObject.Game game;
    }
}
