package com.example.model;

import java.util.List;

public class CategoryAdvice {
    public String code;
    public String title;
    public int index;
    public String route;
    public String icon;
    public int lines;
    public String style;
    public String type;
    public String promotionLink;
    public List<GameAdvice> games;
}
