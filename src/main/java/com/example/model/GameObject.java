package com.example.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GameObject {
    public Game game;

    public static class Game implements Comparable<Game> {
        @JsonProperty("game_code")
        public String gameCode;
        public int rank;

        @Override
        public int compareTo(Game other) {
            return Integer.compare(rank, other.rank);
        }
    }
}
