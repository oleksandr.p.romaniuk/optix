package com.example.model;

public class AreaObject {
    public Area area;

    public static class Area {
        public Integer index;
        public Placement placement;
    }

}
