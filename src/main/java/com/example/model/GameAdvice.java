package com.example.model;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GameAdvice {
    public final String code;
    public final int rank;
}
