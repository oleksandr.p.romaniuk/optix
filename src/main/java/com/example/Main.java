package com.example;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.example.model.AreaObject;
import com.example.model.CategoryAdvice;
import com.example.model.GameAdvice;
import com.example.model.GraphyteLayoutResponse;
import com.example.model.Placement;
import com.example.model.Recommendation;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

public class Main {

    public static void main(String[] args) {
        String uri = "https://api.opti-x.optimove.net/intelligentlayouts/v1/layouts/9ac3f7fd-d5d3-4be8-bb90-c5d07cbef024/layout";
        String requestBody = "{\"userId\":\"e09ccb69/889953251\",\"type\":\"layout\",\"context\":{\"channel\":\"Mobile\"},\"recommendation\":{\"target\":{\"getDetails\":true}}}";

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .header("x-nolog", "1")
                .header("x-brand-key", "227e2253-4622-43a2-905f-44ca9bb4ccd9")
                .header("x-api-key", "3p8Kbemjdq5b3pELp2i6E1vOa1oqHMaa2UQod3cJ")
                .header("Content-Type", "application/json")
                .POST(BodyPublishers.ofString(requestBody))
                .build();

        try {
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

            System.out.println("Response status code: " + response.statusCode());

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            String body = response.body();
            GraphyteLayoutResponse resp = objectMapper.readValue(response.body(), GraphyteLayoutResponse.class);

            var categories = resp.layout.result.stream()
                    .map(it -> it.area)
                    .filter(Main::hasExtraInfo)
                    .map(Main::toCategoryAdvice)
                    .toList();

            boolean hasFilledCategory = categories.stream().anyMatch(c -> !c.games.isEmpty());
            if (hasFilledCategory) {
                //logger.debug("Received '{}' number of game categories", categories.size());
                //return categories;
            } else {
                //logger.debug("Received no game categories");
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static boolean hasExtraInfo(AreaObject.Area area) {
        return Objects.nonNull(area) && Objects.nonNull(area.placement) && Objects.nonNull(area.placement.extra);
    }
    private static CategoryAdvice toCategoryAdvice(AreaObject.Area area) {
        var placement = area.placement;
        var info = placement.extra;
        var advice = new CategoryAdvice();
        advice.code = info.code;
        advice.title = placement.title;
        advice.index = area.index;
        advice.route = info.route;
        advice.icon = info.icon;
        advice.lines = info.lines;
        advice.style = info.style;
        advice.type = info.type;
        advice.games = getGameAdvises(placement);
        advice.promotionLink = info.promotionUrl;
        return advice;
    }

    private static CategoryAdvice toCategoryAdvice(Recommendation recommendation, String productCategoryCode) {
        var advice = new CategoryAdvice();
        advice.code = productCategoryCode;
        advice.games = getGameAdvises(recommendation);

        return advice;
    }

    private static List<GameAdvice> getGameAdvises(Placement placement) {
        return placement.result.stream()
                .map(it -> new GameAdvice(it.game.gameCode, it.game.rank))
                .collect(Collectors.toList());
    }

    private static List<GameAdvice> getGameAdvises(Recommendation recommendation) {
        return recommendation.result.stream()
                .map(it -> new GameAdvice(it.game.gameCode, it.game.rank))
                .collect(Collectors.toList());
    }
}
